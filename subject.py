from tools import get_subject_short_name


class Subject:
    def __init__(self,subject_str):
        self.name = subject_str.strip()
        self.short_name = get_subject_short_name(self.name)
    def __eq__(self, other):
        return self.name == other.name
    def __hash__(self):
        return  hash(self.name)