
class Lesson:
    def __init__(self,subject,lesson_number,day_number,room,format,teachers,weeks,group):
        self.name = subject.name
        self.short_name = subject.short_name
        self.lesson_time = lesson_number
        self.day = day_number
        self.room_data = room
        self.format = format
        self.teachers = teachers
        self.weeks = weeks
        if str(group) != "лекція":
            self.subgroup = group