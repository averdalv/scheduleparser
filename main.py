import json
from openpyxl import load_workbook
import pandas as pd
import os

from classroom import Classroom
from faculty import Faculty
from group import Group
from lesson import Lesson
from subject import Subject
from teacher import Teacher
from tools import *

teachers = []
lessons = []
subjects = []
faculties = []



def handle_row(day_number,lesson_number,subject_str,teacher_str,group_str,week_str,room_str):
    teacher = Teacher(teacher_str)
    subject = Subject(subject_str)
    is_lection = False
    group = parse_group(group_str)
    if not is_integer(group):
        if group == "лекція":
            is_lection = True
    weeks = parse_weeks(week_str)
    room = Classroom(room_str)
    teachers_l = []
    if not teacher.is_empty_teacher():
        teachers_l.append(teacher)
        if teacher not in teachers:
            teachers.append(teacher)
    if subject not in subjects:
        subjects.append(subject)
    format = get_fomat_by_subject(subject.name,is_lection)
    lesson = Lesson(subject,lesson_number,day_number,room,format,teachers_l,weeks,group)
    lessons.append(lesson)

def handle_excel_file(filename):
    wb = load_workbook(filename)
    worksheet = wb.worksheets[0]
    df = pd.DataFrame(worksheet.values)
    faculty_name,group_name = parse_faculty_and_group(df)
    faculty = Faculty(faculty_name)
    group = Group(group_name)
    faculty.add_group(group)
    if faculty not in faculties:
        faculties.append(faculty)
    else:
        f = faculties[faculties.index(faculty)]
        f.add_group(group)
    row_index_of_first_day = findIndexes(df, "Понеділок")[0][1]
    i = row_index_of_first_day
    day_number = 0
    lesson_number = 0
    teachers.clear()
    lessons.clear()
    subjects.clear()
    while True:
        if df[DAY_COL][i] is not None:
            day_number += 1
            lesson_number = 0
        if df[TIME_COL][i] is not None:
            lesson_number += 1
        if df[SUBJECT_COL][i] is not None:
            handle_row(day_number, lesson_number, df[SUBJECT_COL][i], df[TEACHER_COL][i], df[GROUP_COL][i],
                       df[WEEK_COL][i], df[ROOM_COL][i])
        i += 1
        if i > 130:
            break
    lessons_times = [str(i + 1) for i in range(lesson_number)]
    result_file = "{}.json".format(group_name)
    result_object = {"teachers": teachers, 'themes': subjects, 'lessons_times': lessons_times, 'lessons': lessons}
    with open(result_file, 'w', encoding="utf-8", newline='\r\n') as f:
        json.dump(result_object, f, ensure_ascii=False, default=obj_dict, indent=2)
def main():
    for file in os.listdir("."):
        if file.endswith(".xlsx"):
            handle_excel_file(file)
    faculties_obj ={"faculties":faculties}
    faculties_file = "faculties.json"
    with open(faculties_file, 'w', encoding="utf-8", newline='\r\n') as f:
        json.dump(faculties_obj, f, ensure_ascii=False, default=obj_dict, indent=2)

if __name__ == "__main__":
    main()