import re


class Classroom:
    def __init__(self,classroom_str):
        self.parse(classroom_str)
    def parse(self,classroom_str):
        arr = re.split(r"\s*-\s*",classroom_str.strip())
        self.housing = arr[0]
        self.room = arr[1]