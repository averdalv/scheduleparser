class Faculty:
    def __init__(self,faculties_name):
        self.faculties_name = faculties_name
        self.group = []
    def add_group(self,group_obj):
        if group_obj not in self.group:
            self.group.append(group_obj)
    def __eq__(self, other):
        return self.faculties_name == other.faculties_name
    def __hash__(self):
        return hash(self.faculties_name)