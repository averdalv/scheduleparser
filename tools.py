import re

from const import *


def parse_weeks(weeks_str):
    if is_integer(weeks_str):
        return weeks_str
    weeks_str = weeks_str.strip()
    if weeks_str == '1,2':
        return 0
    arr = weeks_str.split(",")
    if len(arr) == 1 and "-" not in arr[0]:
        return int(arr[0])
    range_list = []
    for num_range in arr:
        if "-" in num_range:
            range_list.extend(parse_range_weeks(num_range))
        else:
            range_list.append(int(num_range))
    result = ""
    for i in range(NUM_WEEKS):
        if (i+1) in range_list:
            result+="1"
        else:
            result+="0"
    return result

def parse_range_weeks(range_weeks):
    c = re.compile(r'(\d+)\s*-\s*(\d+)')
    m = c.match(range_weeks)
    if m:
        left_edge = int(m.group(1))
        right_edge = int(m.group(2))
        range_list = []
        while left_edge <= right_edge:
            range_list.append(left_edge)
            left_edge+=1
        return range_list
    else:
        raise RuntimeError("Wrong format for weeks range")


def get_subject_short_name(subject_name):
    if "-" in subject_name:
        return get_subject_short_name(re.split("\s*-\s*",subject_name.strip())[0])
    subject_name_upper = subject_name.strip().upper()
    arr = re.split("\s+",subject_name_upper)
    if len(arr) == 1:
        return  subject_name
    ignore_words = ["ДО","А","ТА","І","Й","АБО"]
    result = ""
    for a in arr:
        if a not in ignore_words:
            result+=a[0]
    return result

def findIndexes(df, value):
    listOfPos = list()
    result = df.isin([value])
    seriesObj = result.any()
    columnNames = list(seriesObj[seriesObj == True].index)
    rows = []
    for col in columnNames:
        rows = list(result[col][result[col] == True].index)
        for row in rows:
            listOfPos.append((col, row))
    return listOfPos

def get_fomat_by_subject(subject_name,is_lection):
    if is_lection:
        return 0
    if subject_name.strip().startswith("Практика"):
        return 2
    return 1
def is_integer(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
def parse_group(group_str):
    if is_integer(group_str):
        return int(group_str)
    return group_str.strip()
def parse_group_name(group_str,is_specialty):
    c = re.compile(r'[А-ЯҐа-яієїґ\'`]+\s\"([А-ЯІЄЇҐа-яієїґ\'`|\s]+)\"\s*,\s*(\d)\s*р.н.')
    m = c.match(group_str)
    if m:
        group_name = get_subject_short_name(m.group(1))
        year = m.group(2)
        if is_specialty:
            return "{0}-{1}".format(group_name,year)
        else:
            return "МП {0}-{1}".format(group_name, year)
    else:
        raise RuntimeError("Wrong format of group name")
def obj_dict(obj):
    return obj.__dict__


def parse_faculty_and_group(df):
    faculty_name = ""
    group_name = ""
    for i in range(100):
        if df[0][i] is not None:
            val = df[0][i].strip()
            if "Факультет" in val:
                faculty_name = re.split("\s+", val)[1].capitalize()
            if "Спеціальність" in val:
                group_name = parse_group_name(val,True)
            elif "МП \"" in val:
                group_name = parse_group_name(val,False)
    return (faculty_name,group_name)