import re


class Teacher:
    def __init__(self,teacher_str):
        self.parse(teacher_str.strip())
    def is_empty_teacher(self):
        return self.first_name == "Вакансія"
    def parse(self,teacher_str):
        if teacher_str == "Вакансія":
            self.first_name = teacher_str
            return
        c = re.compile(r'(.+)\s*([А-ЯІЄЇҐа-яієїґ])\.([А-ЯІЄЇҐа-яієїґ])[\.,]\s?([А-ЯІЄЇҐа-яієїґ\']+)')
        m = c.match(teacher_str)
        if m:
            degree_str = m.group(1)
            self.first_name = m.group(2)
            self.middle_name = m.group(3)
            self.last_name = m.group(4)
            self.degree = self.convert_degree(degree_str)
        else:
            raise RuntimeError("Wrong format of teacher ")
    def convert_degree(self,degree_str):
        degree_str = degree_str.strip()
        degree_obj = {'cт.викл.':"1",
                      'доц.':"2",
                      'проф.':"3",
                      'ас.':"4",}
        return degree_obj.get(degree_str,"5")
    def __eq__(self, other):
        if self.is_empty_teacher() or other.is_empty_teacher():
            return self.is_empty_teacher() and other.is_empty_teacher()
        return self.first_name == other.first_name and self.last_name == other.last_name and self.middle_name == other.middle_name and self.degree == other.degree

    def __hash__(self):
        if self.is_empty_teacher():
            return hash(0)
        else:
            return hash((self.first_name,self.middle_name,self.last_name,self.degree))