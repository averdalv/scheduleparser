class Group:
    def __init__(self,group_name):
        self.group_name = group_name
    def __eq__(self, other):
        return self.group_name == other.group_name
    def __hash__(self):
        return hash(self.group_name)
